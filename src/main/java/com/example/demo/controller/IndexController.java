package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/")
public class IndexController {

    @GetMapping(value = "/")
    public String index(Model model){
        model.addAttribute("page", "home");
        return "index";
    }

    @GetMapping(value = "/index")
    public String backIndexPage(Model model){
        model.addAttribute("page", "home");
        return "index";
    }

}
