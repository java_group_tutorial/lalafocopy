package com.example.demo.dao;
import com.example.demo.domain.Elan;

public interface ElanDao {
    Elan register(Elan elan);
}
