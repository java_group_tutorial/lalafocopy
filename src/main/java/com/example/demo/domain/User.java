package com.example.demo.domain;

import java.math.BigDecimal;

public class User {
   // private long id;
    private String name;
    private String surname;
    private String email;
    private String password;
    private String passconfirmation;
    private String phone;

    public User() {

    }

    public User(String name, String surname, String email, String password) {
        //this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        //this.passconfirmation = passconfirmation;
    }

//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getPassconfirmation() {
        return passconfirmation;
    }

    public void setPassconfirmation(String passconfirmation) {
        this.passconfirmation = passconfirmation;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}

