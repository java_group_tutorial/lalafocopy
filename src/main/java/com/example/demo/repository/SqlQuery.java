package com.example.demo.repository;

public class SqlQuery {
    public static final String INSERT_USER = "INSERT INTO public.bizim_admin( " +
            " id, name, surname, email, password)" +
            " VALUES (nextval('admin_seq'), ?, ?, ?, ?)";
    public static final String AUTHENTICATE_USER = "SELECT email, password " +
            "FROM public.bizim_admin " +
            " where email=? and password=?";
}
