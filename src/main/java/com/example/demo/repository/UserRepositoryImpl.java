package com.example.demo.repository;

import com.example.demo.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class UserRepositoryImpl {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public User insert(User user){
        Object[] data = {user.getName(), user.getSurname(),user.getEmail(),user.getPassword()};
        int count = jdbcTemplate.update(SqlQuery.INSERT_USER, data);
        System.out.println("insert count = " + count);
        return user;
    }
    public User authenticate (User user){
        Object[] data = {user.getEmail(),user.getPassword()};
          jdbcTemplate.update(SqlQuery.AUTHENTICATE_USER,data);
//        String sql=SqlQuery.AUTHENTICATE_USER;
//        List<Map<String, Object>> count = jdbcTemplate.queryForList(sql,data);
        //System.out.println("count = " + count);
        return user;
    }
}
