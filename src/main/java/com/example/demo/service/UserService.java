package com.example.demo.service;


import com.example.demo.domain.User;
import org.springframework.stereotype.Service;

import java.util.Optional;

public interface UserService {
    User insert(User user);
    User authenticate(User user);
}
