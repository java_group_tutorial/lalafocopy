package com.example.demo.service.implementation;


import com.example.demo.domain.User;
import com.example.demo.repository.UserRepositoryImpl;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepositoryImpl userRepository;

    @Override
    public User insert(User user) {
        return userRepository.insert(user);
    }

    @Override
    public User authenticate(User user) {
        return userRepository.authenticate(user);
    }
}
